package fr.sparna.rdf.skos.xls2skos.cli;


public interface CliCommandIfc {

	public void execute(Object args) throws Exception;
	
}
