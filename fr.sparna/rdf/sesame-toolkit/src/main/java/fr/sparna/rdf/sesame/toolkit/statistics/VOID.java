package fr.sparna.rdf.sesame.toolkit.statistics;

public final class VOID {

	/**
	 * The namespace of the VOID vocabulary : http://rdfs.org/ns/void#
	 **/	
	public static final String NAMESPACE = "http://rdfs.org/ns/void#";
	
	public static final String PREFIX = "void";

	// Classes...
	
	public static final String DATASET = NAMESPACE+"Dataset";
	
	public static final String DATASET_DESCRIPTION = NAMESPACE+"DatasetDescription";
	
	public static final String LINKSET = NAMESPACE+"Linkset";
	
	public static final String TECHNICAL_FEATURE = NAMESPACE+"TechnicalFeature";
	
	// Properties ...
	
	public static final String CLASS = NAMESPACE+"class";
	
	public static final String CLASS_PARTITION = NAMESPACE+"classPartition";
	
	public static final String CLASSES = NAMESPACE+"classes";
	
	public static final String DATA_DUMP = NAMESPACE+"dataDump";
	
	public static final String DISTINCT_OBJECTS = NAMESPACE+"distinctObjects";
	
	public static final String DISTINCT_SUBJECTS = NAMESPACE+"distinctSubjects";
	
	public static final String DOCUMENTS = NAMESPACE+"documents";
	
	public static final String ENTITIES = NAMESPACE+"entities";
	
	public static final String EXAMPLE_RESOURCE = NAMESPACE+"exampleResource";
	
	public static final String FEATURE = NAMESPACE+"feature";
	
	public static final String IN_DATASET = NAMESPACE+"inDataset";
	
	public static final String LINK_PREDICATE = NAMESPACE+"linkPredicate";
	
	public static final String OBJECTS_TARGET = NAMESPACE+"objectsTarget";
	
	public static final String OPEN_SEARCH_DESCRIPTION = NAMESPACE+"openSearchDescription";
	
	public static final String PROPERTIES = NAMESPACE+"properties";
	
	public static final String PROPERTY = NAMESPACE+"property";
	
	public static final String PROPERTY_PARTITION = NAMESPACE+"propertyPartition";
	
	public static final String ROOT_RESOURCE = NAMESPACE+"rootResource";
	
	public static final String SPARQL_ENDPOINT = NAMESPACE+"sparqlEndpoint";
	
	public static final String SUBJECTS_TARGET = NAMESPACE+"subjectsTarget";
	
	public static final String SUBSET = NAMESPACE+"subset";
	
	public static final String TARGET = NAMESPACE+"target";
	
	public static final String TRIPLES = NAMESPACE+"triples";
	
	public static final String URI_LOOKUP_ENDPOINT = NAMESPACE+"uriLookupEndpoint";
	
	public static final String URI_REGEX_PATTERN = NAMESPACE+"uriRegexPattern";
	
	public static final String URI_SPACE = NAMESPACE+"uriSpace";
	
	public static final String VOCABULARY = NAMESPACE+"vocabulary";
}
