package fr.sparna.rdf.datapress.web;

public class DashboardData {

	public static final String KEY = DashboardData.class.getCanonicalName();
	
	protected String endpoint;

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
}
